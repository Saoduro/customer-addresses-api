var jwt_token = context.getVariable("request.header.authorization");
var partyId = context.getVariable("request.queryparam.partyId");
var email = context.getVariable("request.queryparam.email");
var addressType = context.getVariable("request.queryparam.addressType");

var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);

var regex = new RegExp(/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.[A-Za-z0-9-_+/=]*$/);
if (regex.test(jwt_token)) {
    context.setVariable("isJwtValid", "true");
}
else{
    context.setVariable("isJwtValid", "false");
}

context.setVariable("isRequestValid","true");

// validations on partyId, addressType and email query params.
if(typeof context.getVariable("request.queryparam.partyId") === 'undefined' || context.getVariable("request.queryparam.partyId") === null){
    context.setVariable("isRequestValid","false");
    context.setVariable("queryparam","partyId");
    context.setVariable("errorMessage","Please provide a valid partyId");
}

var addressTypes =  ["MAILING","SERVICE", "TAX"]
if(addressType !== null && addressTypes.indexOf(addressType) === -1) {
    context.setVariable("isRequestValid","false");
    context.setVariable("queryparam","addressType");
    context.setVariable("errorMessage","Please provide a valid addressType");
}

var emailRegex = RegExp(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/);
if(email !== null && !emailRegex.test(context.getVariable("request.queryparam.email"))) {
    context.setVariable("isRequestValid","false");
    context.setVariable("queryparam","email");
    context.setVariable("errorMessage","Please provide a valid email");
}

var path = "/customers/"+partyId+"/addresses"
print(path)

context.setVariable("apiPath",path);
